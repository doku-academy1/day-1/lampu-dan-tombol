import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int lampu;
        Boolean jawaban = false;
        String statusLampu = "";
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Masukkan banyak lampu: ");
        lampu = keyboard.nextInt();

        int i = 1;

        while (i <= lampu) {
            if (lampu % i == 0) {
                if (jawaban) {
                    jawaban = false;
                } else {
                    jawaban = true;
                }
            }
            i += 1;
        }

        if (jawaban) {
            statusLampu = "Lampu Hidup";
        } else {
            statusLampu = "Lampu Mati";
        }

        System.out.println(statusLampu);

    }
}